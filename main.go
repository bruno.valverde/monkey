package main

import (
	"fmt"
	"monkey/repl"
	"os"
	"os/user"
)

func main() {
	user, err := user.Current()
	if err != nil {
		panic(err)
	}
	fmt.Printf("Hello %s! Bienvenidos a Monkey\n", user.Username)
	fmt.Printf("Empiece a codear")
	repl.Start(os.Stdin, os.Stdout)
}
